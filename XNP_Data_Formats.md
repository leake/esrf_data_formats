This file documents the requirements for data formats of the beamlines of the XNP Group :

**ID01 ID13 ID16A ID16B ID21**

Please provide input on the following questions.

-----------------------------------------------------------

1. **Which analysis programs do you use which read hdf5?**
    
    * name - program description / link


-----------------------------------------------------------

2. **What programs do you and/or users use which do not read hdf5 ?**
	
    * name - format - program description / link


-----------------------------------------------------------

3. **What other data formats do you need for your analysis?**
	
    * name - format description / link


-----------------------------------------------------------

4. **Are these formats required during or after the experiment?**

    * format - online / offline / user format


-----------------------------------------------------------


5. **What issues (if any) do you have with hdf5?**
	
    * issue - description


-----------------------------------------------------------


6. **Which metadata definitions do you need for experimental techniques on your beamline?**

    * technique - description


-----------------------------------------------------------

